let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let LocationSchema = new Schema({
    name: { type: String, unique: true, required: true },
    description: { type: String }
});

module.exports = mongoose.model('Location', LocationSchema, 'Location');