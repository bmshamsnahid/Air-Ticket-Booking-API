let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let AirplaneSchema = new Schema({
    name: { type: String, unique: true, required: true },
    description: { type: String }
});

module.exports = mongoose.model('Airplane', AirplaneSchema, 'Airplane');