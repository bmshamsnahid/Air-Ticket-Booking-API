let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let SeatSchema = new Schema({
    seatNo: { type: String, required: true },
    airplaneId: { type: String, required: true },
    isBooked: { type: Boolean, required: true, default: false },
    isBusinessClass: { type: Boolean, required: true }
});

module.exports = mongoose.model('Seat', SeatSchema, 'Seat');