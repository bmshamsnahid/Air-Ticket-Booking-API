let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let BookSeatSchema = new Schema({
    userId: { type: String, required: true },
    seatId: { type: String, required: true },
    airInfoId: { type: String, required: true },
});

module.exports = mongoose.model('BookSeat', BookSeatSchema, 'BookSeat');