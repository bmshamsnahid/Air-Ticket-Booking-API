let mongoose = require('mongoose'),
    Schema = mongoose.Schema;

let AirInfoSchema = new Schema({
    name: { type: String },
    description: { type: String },
    sourceLocationId: { type: String, required: true },
    destinationLocationId: { type: String, required: true },
    airplaneId: { type: String, required: true },
    fare: { type: String, required: true },
    dates: { type: String, required: true },
    time: { type: String, required: true },
});

module.exports = mongoose.model('AirInfo', AirInfoSchema, 'AirInfo');