module.exports = {
    'serverPort': 8080,
    'tokenexp': 3600,
    'secret': 'mysecretkey',
    'database': 'mongodb://localhost:27017/air-ticket-booking-api'
};
