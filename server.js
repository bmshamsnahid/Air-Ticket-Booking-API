let express = require('express'),
    app = express(),
    bodyParser = require('body-parser'),
    morgan = require('morgan'),
    mongoose = require('mongoose'),
    cors = require('cors'),
    path = require('path'),
    cookieParser = require('cookie-parser');

let config = require('./config');

let port = process.env.PORT || config.serverPort;

mongoose.Promise = global.Promise;
mongoose.connect(config.database, (err) => {
    if (err) {
        console.log('Error in database connection. ' + err);
    } else {
        console.log('Database connected');
    }
});

app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(cors());

let airInfoRoutes = require('./route/airInfo'),
    airplaneRoutes = require('./route/airplane'),
    bookSeatRoutes = require('./route/bookSeat'),
    locationRoutes = require('./route/location'),
    seatRoutes = require('./route/seat'),
    authRoutes = require('./route/auth'),
    userRoutes = require('./route/user'),
    stripeRoutes = require('./route/stripe');

app.use('/api/test', (req, res, next) => {
    return res.status(201).json({ success: true, message: "Well come to air traffic booking api." });
});

app.use('/api/airInfo', airInfoRoutes);
app.use('/api/airplane', airplaneRoutes);
app.use('/api/bookSeat', bookSeatRoutes);
app.use('/api/location', locationRoutes);
app.use('/api/seat', seatRoutes);
app.use('/api/auth', authRoutes);
app.use('/api/user', userRoutes);
app.use('/api/stripe', stripeRoutes);

app.use('*', (req, res, next) => {
    res.status(200).json({ success: false, message: 'Does not match any resource of the routing.' });
});

app.listen(port, (err) => {
    if (err) {
        console.log('Error in listing port: ' + port);
    } else {
        console.log('App is running in port: ' + port);
    }
});
