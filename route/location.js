let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    locationController = require('../controller/location');

router.post('/', authController.adminAuthenticate, locationController.createLocation);
router.get('/:locationId', locationController.getSingleLocation);
router.get('/', locationController.getAllLocation);
router.patch('/:locationId', authController.adminAuthenticate, locationController.updateLocation);
router.delete('/:locationId', authController.adminAuthenticate, locationController.deleteLocation);

module.exports = router;