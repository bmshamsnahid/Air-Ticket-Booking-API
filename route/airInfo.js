let express = require('express'),
    router = express.Router(),
    airInfoController = require('../controller/airInfo'),
    authController = require('../controller/auth');

router.get('/airDetails', airInfoController.getAirDetails);

router.post('/', authController.adminAuthenticate, airInfoController.createAirInfo);
router.get('/:airInfoId', airInfoController.getSingleAirInfo);
router.get('/', airInfoController.getAllAirInfo);
router.patch('/:airInfoId', authController.adminAuthenticate, airInfoController.updateAirInfo);
router.delete('/:airInfoId', authController.adminAuthenticate, airInfoController.deleteAirInfo);

module.exports = router;