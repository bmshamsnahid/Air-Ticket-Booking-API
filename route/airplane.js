let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    airplaneController = require('../controller/airplane');

router.post('/', authController.adminAuthenticate, airplaneController.createAirplane);
router.get('/:airplaneId', airplaneController.getSingleAirplane);
router.get('/', airplaneController.getAllAirplane);
router.patch('/:airplaneId', authController.adminAuthenticate, airplaneController.updateAirplane);
router.delete('/:airplaneId', authController.adminAuthenticate, airplaneController.deleteAirplane);

module.exports = router;