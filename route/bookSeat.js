let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    bookSeatController = require('../controller/bookSeat');

router.get('/bookSeatInfo/:bookSeatId', authController.userAuthenticate, bookSeatController.getBookSeatInfo);

router.post('/', authController.userAuthenticate, bookSeatController.createBookSeat);
router.get('/:bookSeatId', authController.userAuthenticate, bookSeatController.getSingleBookSeat);
router.get('/', authController.userAuthenticate, bookSeatController.getAllBookSeat);
router.patch('/:bookSeatId',  authController.userAuthenticate, bookSeatController.updateBookSeat);
router.delete('/:bookSeatId', authController.userAuthenticate, bookSeatController.deleteBookSeat);

module.exports = router;