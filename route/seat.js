let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    seatController = require('../controller/seat');

router.post('/airplaneSeatsGenerate/:airplaneId', seatController.createAirplaneSeats);
router.get('/airplane/:airplaneId', seatController.getAllSeatsOfAirplane);
router.get('/getAllBookedSeatsOfAirplane/:airplaneId', seatController.getAllBookedSeatsOfAirplane);
router.get('/getAllEmptySeatsOfAirplane/:airplaneId', seatController.getAllEmptySeatsOfAirplane);
router.get('/getAllBookedBusinessClassSeatsOfAirplane/:airplaneId', seatController.getAllBookedBusinessClassSeatsOfAirplane);
router.get('/getAllEmptyBusinessClassSeatsOfAirplane/:airplaneId', seatController.getAllEmptyBusinessClassSeatsOfAirplane);
router.get('/getAllBookedEconomyClassSeatsOfAirplane/:airplaneId', seatController.getAllBookedEconomyClassSeatsOfAirplane);
router.get('/getAllEmptyEconomyClassSeatsOfAirplane/:airplaneId', seatController.getAllEmptyEconomyClassSeatsOfAirplane);

router.post('/', authController.adminAuthenticate, seatController.createSeat);
router.get('/:seatId', seatController.getSingleSeat);
router.get('/', seatController.getAllSeat);
router.patch('/:seatId', authController.adminAuthenticate, seatController.updateSeat);
router.delete('/:seatId', authController.adminAuthenticate, seatController.deleteSeat);

module.exports = router;