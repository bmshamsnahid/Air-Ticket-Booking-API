let express = require('express'),
    router = express.Router(),
    authController = require('../controller/auth'),
    userController = require('../controller/user');

router.post('/', userController.createUser);
router.get('/:userId', userController.getSingleUser);
router.get('/', authController.adminAuthenticate, userController.getAllUser);
router.patch('/:userId', authController.userAuthenticate, userController.updateUser);
router.delete('/:userId', authController.adminAuthenticate, userController.deleteUser);

module.exports = router;