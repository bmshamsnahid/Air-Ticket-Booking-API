let BookSeat = require('../model/bookSeat'),
    User = require('../model/user'),
    Seat = require('../model/seat'),
    Airplane = require('../model/airplane'),
    AirInfo = require('../model/airInfo');

let createBookSeat = (req, res, next) => {
    let userId = req.body.userId,
        seatId = req.body.seatId,
        airInfoId = req.body.airInfoId;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!seatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    }  else if (!airInfoId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        let bookSeat = new BookSeat({
            userId: userId,
            seatId: seatId,
            airInfoId: airInfoId
        });
        bookSeat.save((err, bookSeat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully booked ticket.', data: bookSeat });
            }
        });
    }
};

let getSingleBookSeat = (req, res, next) => {
    let bookSeatId = req.params.bookSeatId;
    if (!bookSeatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        BookSeat.findById(bookSeatId, (err, bookSeat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get booked seat information.', data: bookSeat });
            }
        });
    }
};

let getAllBookSeat = (req, res, next) => {
    BookSeat.find({}, (err, bookSeats) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully get all booked seat information.', data: bookSeats });
        }
    });
};

let updateBookSeat = (req, res, next) => {
    let bookSeatId = req.params.bookSeatId,
        userId = req.body.userId,
        seatId = req.body.seatId,
        airInfoId = req.body.airInfoId;
    console.log('Update section');
    if (!bookSeatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!seatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!airInfoId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        BookSeat.findById(bookSeatId, (err, bookSeat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                bookSeat.userId = userId || bookSeat.userId;
                bookSeat.seatId = seatId || bookSeat.seatId;
                bookSeat.airInfoId = airInfoId || bookSeat.airInfoId;
                bookSeat.save((err, bookSeat) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully updated booked seat information.', data: bookSeat });
                    }
                });
            }
        });
    }
};

let deleteBookSeat = (req, res, next) => {
    let bookSeatId = req.params.bookSeatId;
    if (!bookSeatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        BookSeat.findByIdAndRemove(bookSeatId, (err, bookSeat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully delete booked seat.', data: bookSeat });
            }
        });
    }
};

let getBookSeatInfo = (req, res, next) => {
    let bookSeatId = req.params.bookSeatId;
    if (!bookSeatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        getCustomBookSeatInfo(bookSeatId, (err, bookSeatInfo) => {
           if (err) {
               return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
           } else {
               return res.status(201).json({ success: true, message: 'Successfully get booked seat full information.', data: bookSeatInfo });
           }
        });
    }
};

module.exports = {
    createBookSeat,
    getSingleBookSeat,
    getAllBookSeat,
    updateBookSeat,
    deleteBookSeat,
    getBookSeatInfo
};

let getCustomBookSeatInfo = (bookSeatId, cb) => {
    BookSeat.findById(bookSeatId, (err, bookSeat) => {
        if (err) {
            return cb (err, null);
        } else {
            let airplaneId,
                userId = bookSeat.userId,
                seatId = bookSeat.seatId,
                airInfoId = bookSeat.airInfoId;
            getCustomUserInfo(userId, (err, user) => {
                if (err) {
                    return cb (err, null);
                } else {
                    getCustomSeatInfo(seatId, (err, seat) => {
                        if (err) {
                            return cb (err, null);
                        } else {
                            getCustomAirInfo(airInfoId, (err, airInfo) => {
                                if (err) {
                                    return cb (err, null);
                                } else {
                                    airplaneId = airInfo.airplaneId;
                                    getCustomAirplaneInfo(airplaneId, (err, airplane) => {
                                        if (err) {
                                            return cb (err, null);
                                        } else {
                                            let myBookSeatInfo = {};
                                            myBookSeatInfo.user = user;
                                            myBookSeatInfo.seat = seat;
                                            myBookSeatInfo.airInfo = airInfo;
                                            myBookSeatInfo.airplane = airplane;
                                            return cb (null, myBookSeatInfo);
                                        }
                                    });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
};

let getCustomUserInfo  = (userId, cb) => {
    User.findById(userId, (err, user) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, user);
        }
    });
};

let getCustomSeatInfo = (seatId, cb) => {
    Seat.findById(seatId, (err, seat) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, seat);
        }
    });
};

let getCustomAirplaneInfo = (airplaneId, cb) => {
    Airplane.findById(airplaneId, (err, airplane) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, airplane);
        }
    });
};

let getCustomAirInfo = (airInfoId, cb) => {
    AirInfo.findById(airInfoId, (err, airInfo) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, airInfo);
        }
    });
};
