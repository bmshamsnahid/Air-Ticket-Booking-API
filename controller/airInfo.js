let AirInfo = require('../model/airInfo'),
    Airplane = require('../model/airplane'),
    Location = require('../model/location'),
    Seat = require('../model/seat');

let createAirInfo = (req, res, next) => {
  let name = req.body.name,
      description = req.body.description,
      sourceLocationId = req.body.sourceLocationId,
      destinationLocationId = req.body.destinationLocationId,
      airplaneId = req.body.airplaneId,
      fare = req.body.fare,
      dates = req.body.dates,
      time = req.body.time;

  if (!name) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!description) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!sourceLocationId) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!destinationLocationId) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!airplaneId) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!fare) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!dates) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else if (!time) {
      return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
  } else {
      let airInfo = new AirInfo({
          name: name,
          description: description,
          sourceLocationId: sourceLocationId,
          destinationLocationId: destinationLocationId,
          airplaneId: airplaneId,
          fare: fare,
          dates: dates,
          time: time
      });
      airInfo.save((err, airInfo) => {
          if (err) {
              return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
          } else {
              return res.status(201).json({ success: true, message: 'Successfully created air info.', data: airInfo });
          }
      });
  }

};

let getSingleAirInfo = (req, res, next) => {
    let airInfoId = req.params.airInfoId;
    if (!airInfoId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        AirInfo.findById(airInfoId, (err, airInfo) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get the airInfo.', data: airInfo });
            }
        });
    }
};

let getAllAirInfo = (req, res, next) => {
    AirInfo.find({}, (err, airInfos) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully get all the airInfos.', data: airInfos });
        }
    });
};

let updateAirInfo = (req, res, next) => {
    let airInfoId = req.params.airInfoId,
        name = req.body.name,
        description = req.body.description,
        sourceLocationId = req.body.sourceLocationId,
        destinationLocationId = req.body.destinationLocationId,
        airplaneId = req.body.airplaneId,
        fare = req.body.fare,
        dates = req.body.dates,
        time = req.body.time;

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!airInfoId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!description) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!sourceLocationId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!destinationLocationId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!fare) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!dates) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!time) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        AirInfo.findById(airInfoId, (err, airInfo) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                airInfo.name = name || airInfo.name;
                airInfo.description = description || airInfo.description;
                airInfo.sourceLocationId = sourceLocationId || airInfo.sourceLocationId;
                airInfo.destinationLocationId = destinationLocationId || airInfo.destinationLocationId;
                airInfo.airplaneId = airplaneId || airInfo.airplaneId;
                airInfo.fare = fare || airInfo.fare;
                airInfo.dates = dates || airInfo.dates;
                airInfo.time = time || airInfo.time;

                airInfo.save((err, airInfo) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully update the airInfo.', data: airInfo });
                    }
                });
            }
        });
    }
};

let deleteAirInfo = (req, res, next) => {
    let airInfoId = req.params.airInfoId;
    if (!airInfoId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        AirInfo.findByIdAndRemove(airInfoId, (err, airInfo) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully deleted the airinfo.', data: airInfo });
            }
        });
    }
};

let getAirDetails = (req, res, next) => {
    AirInfo.find((err, airInfos) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            retrieveAirDetails(airInfos, (err, airDetails) => {
                if (err) {
                    return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                } else {
                    return res.status(201).json({ success: true, message: 'Successfully get all the airdetails.', data: airDetails });
                }
            });
        }
    });
};

module.exports = {
  createAirInfo,
  getSingleAirInfo,
  getAllAirInfo,
  updateAirInfo,
  deleteAirInfo,
  getAirDetails
};

let retrieveAirDetails = (airInfos, cb) => {
    console.log('getting air details data');
    let length = airInfos.length;
    let airDetails = [];
    for (let index=0; index<length; index++) {
        let airDetail = {};
        let airInfo = airInfos[index];
        airDetail.airInfo = airInfo;
        getCustomAirplaneInfo(airInfo.airplaneId, (err, airplane) => {
           if (err) {
               return cb (err, null);
           } else {
               airDetail.airplane = airplane;
               getCustomAirplaneSeats(airInfo.airplaneId, (err, seats) => {
                   if (err) {
                      return cb (err, null);
                   } else {
                       airDetail.seats = seats;
                       getCustomLocationInfo(airInfo.sourceLocationId, (err, sourceLocation) => {
                            if (err) {
                                return cb (err, null);
                            } else {
                                airDetail.sourceLocation = sourceLocation;
                                getCustomLocationInfo(airInfo.destinationLocationId, (err, destinationLocation) => {
                                    if (err) {
                                        return cb (err, null);
                                    } else {
                                        airDetail.destinationLocation = destinationLocation;
                                        airDetails.push(airDetail);
                                        if (index == length - 1) {
                                            return cb (null, airDetails);
                                        }
                                    }
                               });
                            }
                       });
                   }
               });
           }
        });
    }
};

let getCustomAirplaneInfo = (airplaneId, cb) => {
    Airplane.findById(airplaneId, (err, airplane) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, airplane);
        }
    });
};

let getCustomAirplaneSeats = (airplaneId, cb) => {
    Seat.find({airplaneId: airplaneId}, (err, seats) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, seats);
        }
    });
};

let getCustomLocationInfo = (locationId, cb) => {
    Location.findById(locationId, (err, location) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, location);
        }
    });
};
