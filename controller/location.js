let Location = require('../model/location');

let createLocation = (req, res, next) => {
    let name = req.body.name,
        description = req.body.description;

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        let location = new Location({
            name: name,
            description: description || ''
        });
        location.save((err, location) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully created the location.', data: location });
            }
        });
    }
};

let getSingleLocation = (req, res, next) => {
    let locationId = req.params.locationId;
    if (!locationId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Location.findById(locationId, (err, location) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get the location.', data: location });
            }
        });
    }
};

let getAllLocation = (req, res, next) => {
    Location.find((err, locations) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully got the locations.', data: locations });
        }
    });
};

let updateLocation = (req, res, next) => {
    let name = req.body.name,
        locationId = req.params.locationId,
        description = req.body.description;

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!locationId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Location.findById(locationId, (err, location) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                location.name = name || location.name;
                location.description = description || location.description;
                location.save((err, location) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully update the location.', data: location });
                    }
                });
            }
        });
    }
};

let deleteLocation = (req, res, next) => {
    let locationId = req.params.locationId;
    if (!locationId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Location.findByIdAndRemove(locationId, (err, location) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully delete the location.', data: location });
            }
        });
    }
};

module.exports = {
    createLocation,
    getSingleLocation,
    getAllLocation,
    updateLocation,
    deleteLocation
};