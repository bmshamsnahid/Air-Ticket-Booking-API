let jwt = require('jsonwebtoken'),
    config = require('../config'),
    User = require('../model/user');

let userLogin = (req, res, next) => {
    let email = req.body.email,
        password = req.body.password;

    if (!email) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete email.'});
    } else if (!password) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete password.'});
    } else {
        User.findOne({email: email}, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else if (!user) {
                return res.status(401).json({ success: false, message: 'No users found.'});
            } else {
                user.comparePassword(password, (err, match) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else if (!match) {
                        return res.status(401).json({ success: false, message: 'Invalid user credentials.'});
                    } else {
                        let token = jwt.sign(user, config.secret, { expiresIn: config.tokenexp });
                        return res.status(201).json({ success: true, data: user, token: token });
                    }
                });
            }
        });
    }
};

let userAuthenticate = (req, res, next) => {
    let token = req.body.token || req.query.token || req.headers['authorization'];

    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                req.decoded = decoded;
                next();
            }
        });
    } else {
        return res.status(202).json({
            success: false,
            message: 'Invalid or incomplete authorization token'
        });
    }
};

let adminAuthenticate = (req, res, next) => {
    console.log('Administrator authentication.');
    let token = req.body.token || req.query.token || req.headers['authorization'];
    if (token) {
        jwt.verify(token, config.secret, (err, decoded) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                let user = decoded._doc;
                if (user.isAdmin) {
                    req.decoded = decoded;
                    next();
                } else {
                    return res.status(202).json({
                        success: false,
                        message: 'Admin Previllages requied.'
                    });
                }
            }
        });
    } else {
        return res.status(202).json({
            success: false,
            message: 'Invalid or incomplete token'
        });
    }
};

module.exports = {
    userLogin,
    userAuthenticate,
    adminAuthenticate
};
