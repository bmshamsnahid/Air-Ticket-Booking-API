let Seat = require('../model/seat');

let createSeat = (req, res, next) => {
    let seatNo = req.body.seatNo,
        airplaneId = req.body.airplaneId,
        isBooked = req.body.isBooked,
        isBusinessClass = req.body.isBusinessClass;

    if (typeof isBooked == 'undefined') {
        isBooked = false;
    }

    if (typeof isBusinessClass == 'undefined') {
        isBusinessClass = false;
    }

    if (!seatNo) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        let seat = new Seat({
            seatNo: seatNo,
            airplaneId: airplaneId,
            isBooked: isBooked,
            isBusinessClass: isBusinessClass
        });
        seat.save((err, seat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully created a seat.', data: seat });
            }
        });
    }
};

let getSingleSeat = (req, res, next) => {
    let seatId = req.params.seatId;
    if (!seatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.findById(seatId, (err, seat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get the seat information.', data: seat });
            }
        });
    }
};

let getAllSeat = (req, res, next) => {
    Seat.find({}, (err, seats) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
        }
    });
};

let updateSeat = (req, res, next) => {
    let seatId = req.params.seatId,
        airplaneId = req.body.airplaneId,
        seatNo = req.body.seatNo,
        isBooked = req.body.isBooked,
        isBusinessClass = req.body.isBusinessClass;

    if (!seatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!seatNo) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (typeof isBooked == 'undefined') {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (typeof isBusinessClass == 'undefined') {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.findById(seatId, (err, seat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                seat.seatNo = seatNo || seat.seatNo;
                seat.airplaneId = airplaneId || seat.airplaneId;
                seat.isBooked = isBooked;
                seat.isBusinessClass = isBusinessClass;
                seat.save((err, seat) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully update the seat.', data: seat });
                    }
                });
            }
        });
    }
};

let deleteSeat = (req, res, next) => {
    let seatId = req.params.seatId;
    if (!seatId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.findByIdAndRemove(seatId, (err, seat) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully delete the seat.', data: seat });
            }
        });
    }
};

let getAllSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllBookedSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: true }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllEmptySeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: false }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllBookedBusinessClassSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: true, isBusinessClass: true }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllEmptyBusinessClassSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: false, isBusinessClass: true }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllBookedEconomyClassSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: true, isBusinessClass: false }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let getAllEmptyEconomyClassSeatsOfAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Seat.find({ airplaneId: airplaneId, isBooked: false, isBusinessClass: false }, (err, seats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get all the seats.', data: seats });
            }
        });
    }
};

let createAirplaneSeats = (req, res, next) => {
    let airplaneId = req.params.airplaneId,
        totalBusinessClassSeats = parseInt(req.body.totalBusinessClassSeats),
        totalEconomyClassSeats = parseInt(req.body.totalEconomyClassSeats);

    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!totalEconomyClassSeats) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!totalBusinessClassSeats) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        generateSeats(totalBusinessClassSeats, totalEconomyClassSeats, airplaneId, (err, airplaneSeats) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully generate all the seats.', data: airplaneSeats });
            }
        });
    }
};

module.exports = {
    createSeat,
    getSingleSeat,
    getAllSeat,
    updateSeat,
    deleteSeat,
    getAllSeatsOfAirplane,
    createAirplaneSeats,
    getAllBookedSeatsOfAirplane,
    getAllEmptySeatsOfAirplane,
    getAllBookedBusinessClassSeatsOfAirplane,
    getAllEmptyBusinessClassSeatsOfAirplane,
    getAllBookedEconomyClassSeatsOfAirplane,
    getAllEmptyEconomyClassSeatsOfAirplane
};

let generateSeats = (bSeats, eSeats, aId, cb) => {
    let tSeats = bSeats + eSeats;
    let airplaneSeats = [];
    for (let index=1; index<=tSeats; index++) {
        let seatNo,
            airplaneId = aId,
            isBooked = false,
            isBusinessClass;
        if (index <= bSeats) {
            seatNo = 'B' + index;
            isBusinessClass = true;
        } else {
            seatNo = 'E' + index;
            isBusinessClass = false;
        }
        let seat = new Seat({
            seatNo: seatNo,
            airplaneId: airplaneId,
            isBooked: isBooked,
            isBusinessClass: isBusinessClass
        });
        createCustomSeat(seat, (err, seat) => {
            if (err) {
                return cb (err, null);
            } else {
                airplaneSeats.push(seat);
                if (index == tSeats) {
                    return cb (null, airplaneSeats);
                }
            }
        });
    }

};

let createCustomSeat = (seat, cb) => {
    seat.save((err, seat) => {
        if (err) {
            return cb (err, null);
        } else {
            return cb (null, seat);
        }
    });
};

