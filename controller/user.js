let User = require('../model/user');

let createUser = (req, res, next) => {
    let name = req.body.name,
        email = req.body.email,
        password = req.body.password,
        isAdmin = req.body.isAdmin;

    if (typeof isAdmin == 'undefined') {
        isAdmin = false;
    }

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!email) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!password) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        let user = new User({
            name: name,
            email: email,
            password: password,
            isAdmin: isAdmin
        });
        user.save((err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully created user.', data: user });
            }
        });
    }
};

let getSingleUser = (req, res, next) => {
    let userId = req.params.userId;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        User.findById(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get user data.', data: user });
            }
        });
    }
};

let getAllUser = (req, res, next) => {
    User.find({}, (err, users) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully get all the users.', data: users });
        }
    });
};

let updateUser = (req, res, next) => {
    let userId = req.params.userId,
        name = req.body.name,
        email = req.body.email,
        isAdmin = req.body.isAdmin;

    if (typeof isAdmin == 'undefined') {
        isAdmin = false;
    }

    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!email) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        User.findById(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                user.name = name || user.name;
                user.email = email || user.email;
                user.isAdmin = isAdmin;
                user.save((err, user) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully updated the user.', data: user });
                    }
                });
            }
        });
    }
};

let deleteUser = (req, res, next) => {
    let userId = req.params.userId;
    if (!userId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        User.findByIdAndRemove(userId, (err, user) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully delete the user.', data: user });
            }
        });
    }
};

module.exports = {
    createUser,
    getSingleUser,
    getAllUser,
    updateUser,
    deleteUser
};