let Airplane = require('../model/airplane');

let createAirplane = (req, res, next) => {
    let name = req.body.name,
        description = req.body.description;

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        let airplane = new Airplane({
            name: name,
            description: description || ''
        });
        airplane.save((err, airplane) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully created the airplane.', data: airplane });
            }
        });
    }
};

let getSingleAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Airplane.findById(airplaneId, (err, airplane) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully get the airplane.', data: airplane });
            }
        });
    }
};

let getAllAirplane = (req, res, next) => {
    Airplane.find((err, airplanes) => {
        if (err) {
            return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
        } else {
            return res.status(201).json({ success: true, message: 'Successfully got the airplanes.', data: airplanes });
        }
    });
};

let updateAirplane = (req, res, next) => {
    let name = req.body.name,
        airplaneId = req.params.airplaneId,
        description = req.body.description;

    if (!name) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Airplane.findById(airplaneId, (err, airplane) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                airplane.name = name || airplane.name;
                airplane.description = description || airplane.description;
                airplane.save((err, airplane) => {
                    if (err) {
                        return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
                    } else {
                        return res.status(201).json({ success: true, message: 'Successfully update the airplane.', data: airplane });
                    }
                });
            }
        });
    }
};

let deleteAirplane = (req, res, next) => {
    let airplaneId = req.params.airplaneId;
    if (!airplaneId) {
        return res.status(202).json({ success: false, message: 'Invalid or incomplete property.'});
    } else {
        Airplane.findByIdAndRemove(airplaneId, (err, airplane) => {
            if (err) {
                return res.status(401).json({ success: false, message: 'Fatal Server Error: ' + err});
            } else {
                return res.status(201).json({ success: true, message: 'Successfully delete the airplane.', data: airplane });
            }
        });
    }
};

module.exports = {
    createAirplane,
    getSingleAirplane,
    getAllAirplane,
    updateAirplane,
    deleteAirplane
};